var readBooksPromise = require('./promise.js');
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
];
 
// Lanjutkan code untuk menjalankan function readBooksPromise
var i = 0;
var timeLimit = 10000;
var numOfBooks = books.length;
var catchError = function(error) {}; // Do nothing

readBooksPromise(timeLimit, books[i])
    .then(function call(sisaWaktu) {
        if(sisaWaktu != timeLimit) {
            i++;
            timeLimit = sisaWaktu;
            if(timeLimit > 0 && i < numOfBooks) {
                readBooksPromise(timeLimit, books[i])
                    .then(call)
                    .catch(catchError);
            } else if(i == numOfBooks) {
                console.log("buku habis");
            }
        }
    })
    .catch(catchError);