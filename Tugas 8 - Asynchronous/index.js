var readBooks = require('./callback.js');
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
];
 
// Tulis code untuk memanggil function readBooks di sini
var i = 0;
var timeLimit = 10000;
var numOfBooks = books.length;

readBooks(timeLimit, books[i], function call(sisaWaktu) {
    if(sisaWaktu != timeLimit) {
        i++;
        timeLimit = sisaWaktu;
        if(timeLimit > 0 && i < numOfBooks) {
            readBooks(timeLimit, books[i], call);
        } else if(i == numOfBooks) {
            console.log("buku habis");
        }
    }
});