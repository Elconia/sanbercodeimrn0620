// Soal If-else
var nama = "Jane";
var peran = "Penyihir";

if(nama == "") {
    console.log("Nama harus diisi!");
}
else {
    if(peran == "") {
        console.log("Pilih Peranmu untuk memulai game");
    }
    else if (peran.toLowerCase() == "penyihir") {
        console.log("Selamat datang di Dunia Werewolf, " + nama);
        console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!");
    }
    else if (peran.toLowerCase() == "guard") {
        console.log("Selamat datang di Dunia Werewolf, " + nama);
        console.log("Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.");
    }
    else if (peran.toLowerCase() == "werewolf") {
        console.log("Selamat datang di Dunia Werewolf, " + nama);
        console.log("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!");
    }
    else {
        console.log("Peran yang kamu masukkan salah");
    }
}

// Soal Switch Case
var tanggal = 1; 
var bulan = 5; 
var tahun = 1945;

if (tanggal >= 1 && tanggal <= 31) {
    if (bulan >= 1 && bulan <= 12) {
        if (tahun >= 1900 && tahun <= 2200) {
            var namaBulan;
            switch(bulan) {
                case 1 : { namaBulan = "Januari"; break; }
                case 2 : { namaBulan = "Februari"; break; }
                case 3 : { namaBulan = "Maret"; break; }
                case 4 : { namaBulan = "April"; break; }
                case 5 : { namaBulan = "Mei"; break; }
                case 6 : { namaBulan = "Juni"; break; }
                case 7 : { namaBulan = "Juli"; break; }
                case 8 : { namaBulan = "Agustus"; break; }
                case 9 : { namaBulan = "September"; break; }
                case 10 : { namaBulan = "Oktober"; break; }
                case 11 : { namaBulan = "November"; break; }
                case 12 : { namaBulan = "Desember"; break; }
            }
            console.log(tanggal + " " + namaBulan + " " + tahun);
        }
        else {
            console.log("Tahun yang diinputkan harus diantara 1900-2200!");
        }
    }
    else {
        console.log("Bulan yang diinputkan harus diantara 1-12!");
    }
}
else {
    console.log("Tanggal yang diinputkan harus diantara 1-31!");
}