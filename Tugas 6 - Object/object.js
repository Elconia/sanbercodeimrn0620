// Soal no 1
function arrayToObject(arr) {
    // Code di sini
    var i = 1;
    var personObj = {};
    var now = new Date();
    var thisYear = now.getFullYear(); // 2020 (tahun sekarang)

    arr.forEach(person => {
        personObj.firstName = person[0];
        personObj.lastName = person[1];
        personObj.gender = person[2];
        if(typeof person[3] === "number" && person[3] <= thisYear)
            personObj.age = thisYear - person[3];
        else
            personObj.age = "Invalid Birth Year";

        process.stdout.write(`${i}. ${personObj.firstName} ${personObj.lastName}: `);
        console.log(personObj);
        i++;
    });
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""
console.log("--------------------------------------------------");

// Soal no 2
function shoppingTime(memberId, money) {
    // you can only write your code here!
    if(!memberId)
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    else if (+money < 50000)
        return "Mohon maaf, uang tidak cukup";

    var shoppingObj = {};
    var productList = [ 
        ["Sepatu Stacattu", 1500000], 
        ["Baju Zoro", 500000], 
        ["Baju H&N", 250000], 
        ["Sweater Uniklooh", 175000], 
        ["Casing Handphone", 50000] 
    ];
    var itemList = [];

    shoppingObj.memberId = memberId;
    shoppingObj.money = money;
    for(var i = 0; i < productList.length; i++) {
        if(money >= productList[i][1]) {
            itemList.push(productList[i][0]);
            money -= productList[i][1];
        }
    }
    shoppingObj.listPurchased = itemList;
    shoppingObj.changeMoney = money;

    return shoppingObj;
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
console.log("--------------------------------------------------");

// Soal no 3
function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    var arrAngkotObj = [];

    arrPenumpang.forEach(penumpang => {
        var angkotObj = {};
        var bayar = 0;

        angkotObj.penumpang = penumpang[0];
        angkotObj.naikDari = penumpang[1];
        angkotObj.tujuan = penumpang[2];
        for(var i = 0; i < rute.length; i++) {
            if(penumpang[1] === rute[i] && penumpang[1] !== penumpang[2]) {
                bayar += 2000;
                penumpang[1] = rute[i+1];
            }
        }
        angkotObj.bayar = bayar;

        arrAngkotObj.push(angkotObj);
    });

    return arrAngkotObj;
}
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]