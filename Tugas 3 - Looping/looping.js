// Soal No 1
var loop1 = 0;
console.log("LOOPING PERTAMA");
while(loop1 < 20) {
    loop1 += 2; 
    console.log(loop1 + " - I love coding");
}
console.log("LOOPING KEDUA");
while(loop1 > 0) {
    console.log(loop1 + " - I will become a mobile developer"); 
    loop1 -= 2;
}
console.log("-------------------------");

// Soal No 2
for(var loop2 = 1; loop2 <= 20; loop2++) {
    if(loop2 % 2 == 1 && loop2 % 3 == 0)
        console.log(loop2 + " - I Love Coding");
    else if(loop2 % 2 == 1)
        console.log(loop2 + " - Santai");
    else if(loop2 % 2 == 0)
        console.log(loop2 + " - Berkualitas");
}
console.log("-------------------------");

// Soal No 3
for(var i = 1; i <= 4; i++) {
    var loop3 = "";
    for(var j = 1; j <= 8; j++)
        loop3 += "#";
    console.log(loop3);
}
console.log("-------------------------");

// Soal No 4
for(var i = 1; i <= 7; i++) {
    var loop4 = "";
    for(var j = 1; j <= i; j++)
        loop4 += "#";
    console.log(loop4);
}
console.log("-------------------------");

// Soal No 5
for(var i = 1; i <= 8; i++) {
    var loop5 = "";
    for(var j = 1; j <= 8; j++)
        loop5 += (i % 2 == 1 && j % 2 == 1) || (i % 2 == 0 && j % 2 == 0) ? " " : "#";
    console.log(loop5);
}