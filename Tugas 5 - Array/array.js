// Soal No 1
function range(startNum, finishNum) {
    var rangeArray = [];

    if(startNum <= finishNum) {
        do {
            rangeArray.push(startNum);
            startNum++;
        } while(startNum <= finishNum);
    } else if(startNum > finishNum) {
        do {
            rangeArray.push(startNum);
            startNum--;
        } while(startNum >= finishNum);
    } else {
        return -1;
    }

    return rangeArray;
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1
console.log("-------------------------")

// Soal No 2
function rangeWithStep(startNum, finishNum, step = 1) {
    var rangeArray = [];

    if(startNum <= finishNum && step > 0) {
        do {
            rangeArray.push(startNum);
            startNum += step;
        } while(startNum <= finishNum);
    } else if(startNum > finishNum  && step > 0) {
        do {
            rangeArray.push(startNum);
            startNum -= step;
        } while(startNum >= finishNum);
    } else {
        return -1;
    }

    return rangeArray;
}

console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5]
console.log("-------------------------");

// Soal No 3
function sum(startNum = 0, finishNum = 0, step = 1) {
    var rangeArray = [];
    
    if(startNum <= finishNum && step > 0) {
        do {
            rangeArray.push(startNum);
            startNum += step;
        } while(startNum <= finishNum);
    } else if(startNum > finishNum  && step > 0) {
        do {
            rangeArray.push(startNum);
            startNum -= step;
        } while(startNum >= finishNum);
    } else {
        rangeArray.push(0);
    }

    var sumArray = rangeArray.reduce(function(a, b){
        return a + b;
    });

    return sumArray;
}

console.log(sum(1,10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15,10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0
console.log("-------------------------");

// Soal no 4
function dataHandling(dataArray) {
    dataArray.forEach(data => {
        console.log("Nomor ID:  " + data[0] + "\n" +
                    "Nama Lengkap:  " + data[1] + "\n" +
                    "TTL:  " + data[2] + " " + data[3]+ "\n" +
                    "Hobi:  " + data[4] + "\n");
    });
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];

dataHandling(input);
console.log("-------------------------");

// Soal no 5
function balikKata(kata) {
    var kataTerbalik = "";

    for(let index in kata)
        kataTerbalik = kata[index] + kataTerbalik;

    return kataTerbalik;
}

console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I
console.log("-------------------------");

// Soal no 6
function dataHandling2(dataArray) {
    dataArray.splice(1, 1, "Roman Alamsyah Elsharawy");
    dataArray.splice(2, 1, "Provinsi Bandar Lampung");
    dataArray.splice(4, 1, "Pria", "SMA Internasional Metro");
    console.log(dataArray);

    var tanggalLahir = dataArray[3].split("/");
    for(let index in tanggalLahir) // Convert string to int
        tanggalLahir.splice(index, 1, +tanggalLahir[index]);

    var bulan = tanggalLahir[1];
    var namaBulan;
    switch(bulan) {
        case 1 : { namaBulan = "Januari"; break; }
        case 2 : { namaBulan = "Februari"; break; }
        case 3 : { namaBulan = "Maret"; break; }
        case 4 : { namaBulan = "April"; break; }
        case 5 : { namaBulan = "Mei"; break; }
        case 6 : { namaBulan = "Juni"; break; }
        case 7 : { namaBulan = "Juli"; break; }
        case 8 : { namaBulan = "Agustus"; break; }
        case 9 : { namaBulan = "September"; break; }
        case 10 : { namaBulan = "Oktober"; break; }
        case 11 : { namaBulan = "November"; break; }
        case 12 : { namaBulan = "Desember"; break; }
    }
    console.log(namaBulan);

    tanggalLahir.sort(function(a, b){return b-a});
    for(let index in tanggalLahir) // Convert int to string
        tanggalLahir.splice(index, 1, tanggalLahir[index].toString().padStart(2, "0"));
    console.log(tanggalLahir);

    console.log(dataArray[3].split("/").join("-"));

    console.log(dataArray[1].slice(0, 15));
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);