import * as React from 'react';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import 'react-native-gesture-handler';

import LoginScreen from './LoginScreen';
import HomeScreen from './HomeScreen';

const Stack = createStackNavigator();
const LoginStack = createStackNavigator();
const HomeStack = createStackNavigator();

const LoginStackScreen = ({ navigation }) => (
  <LoginStack.Navigator>
    <LoginStack.Screen name='Login' component={LoginScreen} onPress={() => navigation} />
  </LoginStack.Navigator>
);

const HomeStackScreen = ({ route }) => (
  <HomeStack.Navigator>
    <HomeStack.Screen name='Login' component={HomeScreen} onPress={() => route} />
  </HomeStack.Navigator>
);

export default class App extends React.Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Login" >
          <Stack.Screen name='Login' component={LoginStackScreen} options={{ headerShown: false }} />
          <Stack.Screen name='Home' component={HomeStackScreen} options={{ headerTitle: 'Daftar Barang' }} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
