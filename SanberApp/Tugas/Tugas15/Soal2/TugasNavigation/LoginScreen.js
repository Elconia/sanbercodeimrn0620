import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class LoginScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.body}> 
          <Image source={require('./images/logo.png')} style={styles.imgBg} />
          <Text style={styles.welcome}>Welcome,</Text>
          <Text style={styles.signInToContinue}>Sign in to continue</Text>
          <View style={styles.txtContainer}>
            <Icon style={styles.loginIcon} name='email' size={25}  />
            <TextInput style={styles.loginTxt} placeholder='Email Address'></TextInput>
          </View>
          <View style={styles.txtContainer}>
            <Icon style={styles.loginIcon} name='lock' size={25} />
            <TextInput style={styles.loginTxt} placeholder='Password' secureTextEntry={true}></TextInput>
          </View>
          <TouchableOpacity style={styles.btnSignIn}>
            <Text style={styles.signIn}>Sign in</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.troubleContainer}>
            <Text style={styles.btnTrouble}>Having trouble signing in?</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.signUpContainer}>
            <Image style={styles.googleIcon} source={require('./images/google-logo.png')} />
            <Text style={styles.txtGoogle}>Sign in with Google</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.signUpContainer}>
            <Text style={styles.txtSignUp}>New User? Sign up</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.aboutContainer}>
            <Text style={styles.txtGoogle}>About me</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },
  body: {
    flex: 1,
    width: '100%',
    flexDirection: 'column',
    padding: 35, paddingTop: 5,
    fontFamily: 'Roboto'
  },
  imgBg: {
    width: '100%', 
    height: 100,
    resizeMode: 'contain'
  },
  welcome: {
    color: '#333333',
    fontSize: 36,
    fontWeight: 'bold',
    marginTop: 10
  },
  signInToContinue: {
    color: '#333333',
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10
  },
  txtContainer: {
    width: '100%',
    height: 45,
    flexDirection: 'row',
    marginTop: 20,
    backgroundColor: '#F3F3F3',
    borderRadius: 6,
    borderWidth: 0.8,
    borderColor: '#c5c5c5',
  },
  loginIcon: {
    width: 50,
    height: 45,
    color: '#8A8A8A',
    paddingTop: 10, paddingLeft: 13
  },
  loginTxt: {
    flex: 1,
    height: 45,
    fontSize: 16
  },
  btnSignIn: {
    width: '100%',
    height: 45,
    marginTop: 25,
    borderRadius: 6,
    backgroundColor: '#5265FF',
    alignItems: 'center',
    justifyContent: 'center'
  },
  signIn: {
    color: '#ffffff',
    fontSize: 18,
    fontWeight: 'bold'
  },
  troubleContainer: {
    width: '100%',
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderColor: '#c5c5c5',
  },
  btnTrouble: {
    color: '#8A8A8A',
    fontWeight: 'bold'
  },
  signUpContainer: {
    width: '100%',
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 15,
    borderRadius: 6,
    borderWidth: 0.8,
    borderColor: '#c5c5c5'
  },
  googleIcon: {
    position: 'absolute',
    left: 12, top: 9,
    width: 25,
    height: 25,
    resizeMode: 'contain'
  },
  txtGoogle: {
    color: '#333333',
    fontSize: 18,
    fontWeight: 'bold'
  },
  txtSignUp: {
    color: '#5265FF',
    fontSize: 18,
    fontWeight: 'bold'
  },
  aboutContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end'
  }
});
