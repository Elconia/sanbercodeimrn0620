import React from 'react';
import { View, StyleSheet} from 'react-native';
import LoginScreen from './TugasNavigation/LoginScreen';
import AboutScreen from './TugasNavigation/AboutScreen';
import SkillScreen from './TugasNavigation/SkillScreen';
import ProjectScreen from './TugasNavigation/ProjectScreen';
import AddScreen from './TugasNavigation/AddScreen';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  }
});

const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

export const Login = ({ navigation }) => (
  <ScreenContainer>
    <LoginScreen />
  </ScreenContainer>
);

export const About = () => (
  <ScreenContainer>
    <AboutScreen />
  </ScreenContainer>
);

export const Skill = () => (
  <ScreenContainer>
    <SkillScreen />
  </ScreenContainer>
);

export const Project = () => (
  <ScreenContainer>
    <ProjectScreen />
  </ScreenContainer>
);

export const Add = () => (
  <ScreenContainer>
    <AddScreen />
  </ScreenContainer>
);