import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, StackView } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';

import { Login, About, Skill, Project, Add } from './Screen';

const AuthStack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const LoginStack = createStackNavigator();
const AboutScreen = createStackNavigator();
const SkillScreen = createStackNavigator();
const ProjectScreen = createStackNavigator();
const AddScreen = createStackNavigator();
const Drawer = createDrawerNavigator();

const LoginStackScreen = () => (
  <LoginStack.Navigator>
    <LoginStack.Screen name='Login' component={Login} />
  </LoginStack.Navigator>
);

const AboutStackScreen = () => (
  <AboutScreen.Navigator>
    <AboutScreen.Screen name='About' component={About} />
  </AboutScreen.Navigator>
);

const SkillStackScreen = () => (
  <SkillScreen.Navigator>
    <SkillScreen.Screen name='Skill' component={Skill} />
  </SkillScreen.Navigator>
);

const ProjectStackScreen = () => (
  <ProjectScreen.Navigator>
    <ProjectScreen.Screen name='Project' component={Project} />
  </ProjectScreen.Navigator>
);

const AddStackScreen = () => (
  <AddScreen.Navigator>
    <AddScreen.Screen name='Add' component={Add} />
  </AddScreen.Navigator>
);

const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name='Skill' component={SkillStackScreen} />
    <Tabs.Screen name='Project' component={ProjectStackScreen} />
    <Tabs.Screen name='Add' component={AddStackScreen} />
  </Tabs.Navigator>
);

export default () => (
  <NavigationContainer>
    <Drawer.Navigator>
      <Drawer.Screen name='Login' component={LoginStackScreen} />
      <Drawer.Screen name='Tabs' component={TabsScreen} />
      <Drawer.Screen name='About' component={AboutStackScreen} />
    </Drawer.Navigator>
  </NavigationContainer>
);