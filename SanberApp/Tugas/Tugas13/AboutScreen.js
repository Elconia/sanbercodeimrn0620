import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Constants from 'expo-constants';

export default class AboutScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.body}> 
          <Image source={require('./src/image/about.png')} style={styles.imgBg} />
          <TouchableOpacity style={styles.backIcon}>
            <Icon name='arrow-left' size={30}  />
          </TouchableOpacity>
          <View style={styles.about}>
            <Text style={styles.aboutTxt}>About me</Text>
          </View>
          <Image source={require('./src/image/profile.jpg')} style={styles.profilePic} />
          <View style={styles.mainDesc}>
            <Text style={styles.namaTxt}>Yohanes Subagio</Text>
            <Text style={styles.description}>Thank you for downloading and using this app!{"\n"}
                                            Don’t look so sour, I’m pleased to meet you.</Text>
            <Text style={styles.description}>I love meeting new people and making new friends!{"\n"} 
                                            If there’s anything you might want to share with me,{"\n"}
                                            feel free to contact me. :)</Text>

            <View style={{...styles.link, ...{marginTop: 25}}}>
              <Icon style={styles.linkIcon} name='email' size={25} />
              <Text style={styles.linkTxt}>yohanes.subagio@gmail.com</Text>
            </View>
            <View style={styles.link}>
              <Icon style={styles.linkIcon} name='facebook' size={25} />
              <Text style={styles.linkTxt}>facebook.com/yohanes.codef</Text>
            </View>
            <View style={styles.link}>
              <Icon style={styles.linkIcon} name='twitter' size={25} />
              <Text style={styles.linkTxt}>twitter.com/yohanessubagio</Text>
            </View>
            <View style={styles.link}>
              <Icon style={styles.linkIcon} name='instagram' size={25} />
              <Text style={styles.linkTxt}>instagram.com/yohanessubagio</Text>
            </View>

            <View style={styles.seeMore}>
              <Text style={styles.seeMoreTxt}>Wanna see more of my work? Visit here!</Text>
              <Text style={styles.linkTxt}>https://gitlab.com/Elconia</Text>
            </View>
          </View>
        </View>
        <StatusBar style="auto" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight
  },
  body: {
    flex: 1,
    width: '100%',
    flexDirection: 'column',
    fontFamily: 'Roboto'
  },
  mainDesc: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    padding: 35
  },
  namaTxt: {
    color: '#333333',
    fontSize: 28,
    fontWeight: 'bold',
    marginTop: 15
  },
  imgBg: {
    width: '100%', 
    height: 180,
    resizeMode: 'stretch'
  },
  backIcon: {
    position: 'absolute',
    top: 10, left: 10
  },
  about: {
    position: 'absolute',
    top: 30,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  aboutTxt: {
    color: '#FFFFFF',
    fontSize: 30,
    fontWeight: 'bold',
  },
  profilePic: {
    position: 'absolute',
    top: 90,
    width: '100%',
    height: 130,
    borderRadius: 250,
    resizeMode: 'contain'
  },
  description: {
    color: '#8A8A8A',
    fontSize: 14,
    textAlign: 'center',
    fontWeight: 'bold',
    marginTop: 15
  },
  link: {
    flexDirection: 'row',
    width: '100%',
    alignItems: 'flex-start',
    marginTop: 15
  },
  linkIcon: {
    color: '#333333'
  },
  linkTxt: {
    color: 'blue',
    marginLeft: 15,
    fontSize: 16
  },
  seeMore: {
    width: '100%',
    alignItems: 'center',
    marginTop: 25,
    borderTopWidth: 1,
    borderColor: '#c5c5c5',
  },
  seeMoreTxt: {
    color: '#333333',
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 15, marginBottom: 10
  }
});
